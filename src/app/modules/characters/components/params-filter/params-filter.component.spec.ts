import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ParamsFilterComponent} from './params-filter.component';
import {CharactersModule} from '../../characters.module';

describe('ParamsFilterComponent', () => {
  let component: ParamsFilterComponent;
  let fixture: ComponentFixture<ParamsFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CharactersModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParamsFilterComponent);
    component = fixture.componentInstance;
    component.resolveParamsByLink = () => null;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
