import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ICharactersListResponse} from '@modules/characters/interfaces/characters-list-response';
import {CharactersService} from '@modules/characters/services/characters.service';
import {IFilms} from '../../interfaces/films';

@Component({
  selector: 'sw-params-filter',
  templateUrl: './params-filter.component.html',
  styleUrls: ['./params-filter.component.scss'],
})
export class ParamsFilterComponent implements OnInit {
  @Input() optionsLink: string;
  @Input() optionTitle: string;
  @Input() filterParamsName: string;
  @Output() filterChange: EventEmitter<string> = new EventEmitter();
  public paramsState: ICharactersListResponse<IFilms>;
  public filterOptions: IFilms[];
  public selectedOption: IFilms;
  public optionsVisible = false;
  constructor(private charactersService: CharactersService) {}

  ngOnInit(): void {
    this.resolveParamsByLink();
  }
  toogleParams(): void {
    this.optionsVisible = !this.optionsVisible;
  }

  loadPreviousPage(): void {
    this.resolveParamsByLink(this.paramsState.previous);
  }
  loadNextPage(): void {
    this.resolveParamsByLink(this.paramsState.next);
  }
  resolveParamsByLink(link = this.optionsLink): void {
    this.charactersService.getParamsByLink<IFilms>(link).subscribe((params) => {
      this.paramsState = params;
      this.filterOptions = params.results;
    });
  }
  selectOption(option: IFilms): void {
    if (this.selectedOption !== option) {
      this.selectedOption = option;
    } else {
      this.selectedOption = null;
    }
    this.filterChange.emit(this.selectedOption?.url);
  }
}
