import {InAppRoutes} from '@modules/shared/classes/in-app.routes';

export class CharactersRoutes extends InAppRoutes {
  public static CHARACTERS_ROOT = 'characters';
  public static CHARACTERS_LIST = 'list';
  public static CHARACTER_INFO = 'info';
}
