import {Component, OnInit} from '@angular/core';
import {Router, Params} from '@angular/router';
import {CharactersStore} from '@modules/characters/store/characters.store';
import {CharactersRoutes} from '../../characters.routes';
import {EndpointsService} from '../../../shared/services/endpoints.service';

@Component({
  selector: 'sw-characters-list',
  templateUrl: './characters-list.component.html',
  styleUrls: ['./characters-list.component.scss'],
})
export class CharactersListComponent implements OnInit {
  public searchParams: Params = {};
  constructor(
    public charactersStore: CharactersStore,
    private router: Router,
    public endpointsService: EndpointsService
  ) {}

  ngOnInit(): void {
    this.charactersStore.loadCharacterPage().subscribe();
  }
  loadPreviousPage(): void {
    this.charactersStore
      .loadCharacterPage(this.charactersStore.charactersState.value.previous)
      .subscribe();
  }
  loadNextPage(): void {
    this.charactersStore
      .loadCharacterPage(this.charactersStore.charactersState.value.next)
      .subscribe();
  }
  openCharacterInfo(index): void {
    this.router.navigate([
      `${CharactersRoutes.route(CharactersRoutes.CHARACTERS_ROOT)}${CharactersRoutes.route(
        CharactersRoutes.CHARACTER_INFO
      )}`,
      index,
    ]);
  }

  applyFilter(filterName: string, filterValue: string): void {
    if (filterName && filterValue) {
      this.searchParams = {...this.searchParams, [filterName]: filterValue};
    } else if (!filterValue) {
      delete this.searchParams[filterName];
    }
    console.log('searchParams', this.searchParams);
  }
}
