import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CharacterInfoComponent} from './character-info.component';
import {RouterTestingModule} from '@angular/router/testing';
import {CharactersModule} from '../../characters.module';
import {CharactersService} from '@modules/characters/services/characters.service';

describe('CharacterInfoComponent', () => {
  let component: CharacterInfoComponent;
  let fixture: ComponentFixture<CharacterInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [CharactersService],
      imports: [RouterTestingModule, CharactersModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
