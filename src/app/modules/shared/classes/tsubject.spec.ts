import {TSubject} from './tsubject';
describe('TSubject:', () => {
  it('should return subject value sync', () => {
    const subject: TSubject<number> = new TSubject(42);
    expect(subject.value).toEqual(42);
  });

  it('should return subject value async', () => {
    const subject: TSubject<number> = new TSubject(42);
    let subjectValue = 0;
    subject.asyncValue().subscribe((value) => {
      subjectValue = value;
    });
    expect(subjectValue).toEqual(42);
  });

  it('should emit last value', () => {
    const subject: TSubject<number> = new TSubject(42);
    subject.value = 43;
    expect(subject.value).toEqual(43);
  });
});
